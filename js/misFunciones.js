async function leerJSON(url) {

    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {

        alert(err);
    }
}

const parametro = new URLSearchParams(window.location.search);
var infoEmpleado = {
    nombreEmpleado: '',
    nombreSucursal: '',
    arrayVentas: [],
    totalVentas: 0
}
var infoSucursal = {
    arrayVentaSucursal: [],
    nombreSucursal: "",
    nombreCiudad: ""
}

var infoVenta = [];

function lecturaDatos(select) {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/empresa.json";
    leerJSON(url).then(datos => {
        drawTable(datos.sucursales, datos.ciudades, select);
        drawGrafica(select);
    })
}


function consultarHtml(flag) {
    if (flag == 1) {
        location.href = 'html/consultarEmpleado.html?codigo=' + document.getElementById('codEmpleado').value + '';
    } else if (flag == 2) {
        location.href = 'html/consultarSucursal.html?codigo=' + document.getElementById('codSucursal').value + '';
    } else if (flag == 3) {
        location.href = 'html/consultarEstadistica.html?';
    }
}

function datosEmpleado(sucursales) {
    let codigoEmpleado = parametro.get('codigo');
    let cnt = 0;
    for (let i = 0; i < sucursales.length; i++) {
        for (let j = 0; j < sucursales[i].empleados.length; j++) {
            if (codigoEmpleado == sucursales[i].empleados[j].codigo_empleado) {
                infoEmpleado.nombreEmpleado = sucursales[i].empleados[j].nombre;
                infoEmpleado.nombreSucursal = sucursales[i].nombre_sucursal;
                cnt++;
                llenarVentasEmpleado(sucursales[i].empleados[j].ventas);
            }
        }
    }
    if (cnt == 0 || cnt >= 2) {
        alert("El código que digito no es valido, por favor digite otro codigo.");
        location.href = '../index.html';
    }
}

function DatosSucursales(sucursales, ciudades) {
    let codigoSucursal = parametro.get('codigo');
    let cnt = 0;
    for (let i = 0; i < sucursales.length; i++) {
        if (sucursales[i].id_sucursal == codigoSucursal) {
            infoSucursal.nombreSucursal = sucursales[i].nombre_sucursal;
            searchCiudad(sucursales[i].id_sucursal, ciudades);
            for (let j = 0; j < sucursales[i].empleados.length; j++) {
                llenarVentasEmpleado(sucursales[i].empleados[j].ventas);
            }
            cnt++;
        }
    }
    if (cnt == 0 || cnt >= 2) {
        alert("El código que digito no es valido, por favor digite otro codigo.");
        location.href = '../index.html';
    } else {
        llenarVentasSucursal(0);
    }
}

function DatosEstadisticas(sucursales, ciudades) {
    let cnt = 0;
    for (let i = 0; i < sucursales.length; i++) {
        infoSucursal.nombreSucursal = sucursales[i].nombre_sucursal;
        searchCiudad(sucursales[i].id_sucursal, ciudades);
        for (let j = 0; j < sucursales[i].empleados.length; j++) {
            llenarVentasEmpleado(sucursales[i].empleados[j].ventas);
        }
        llenarVentasSucursal(1);
    }

}

function searchCiudad(id_ciudad, ciudaes) {
    for (let i = 0; i < ciudaes.length; i++) {
        if (ciudaes[i].id_ciudad == id_ciudad) {
            infoSucursal.nombreCiudad = ciudaes[i].nombre_ciudad;
        }
    }
}

function llenarVentasSucursal(flag) {
    for (let i = 0; i < 12; i++) {
        let value = 0;
        let arraySucursal = [];
        for (let j = 0; j < infoEmpleado.arrayVentas.length; j++) {
            let empleado = infoEmpleado.arrayVentas[j];
            if (addMes(i) == empleado[0]) {
                value += empleado[1];
                infoEmpleado.arrayVentas.splice(j, 1);
            }
        }
        arraySucursal.push(addMes(i));
        arraySucursal.push(value);
        infoSucursal.arrayVentaSucursal.push(arraySucursal);
    }
    if (flag == 1)
        llenarVentas();
}

function llenarVentasEmpleado(ventas) {
    for (let i = 0; i < ventas.length; i++) {
        let ventaEmpleado = ventas[i];
        let ventasArray = [];
        ventasArray.push(addMes(ventaEmpleado.id_mes - 1));
        ventasArray.push(ventaEmpleado.valor_venta);
        infoEmpleado.totalVentas += ventaEmpleado.valor_venta;
        infoEmpleado.arrayVentas.push(ventasArray);
    }
}
let array = [];

function llenarVentas() {
    let value = 0;

    for (let i = 0; i < infoSucursal.arrayVentaSucursal.length; i++) {
        value += infoSucursal.arrayVentaSucursal[i][1];
    }
    infoVenta.push(infoSucursal.nombreSucursal);
    infoVenta.push(value);
    array.push(infoVenta);
    infoSucursal.arrayVentaSucursal = [];
    infoVenta = [];
}

function addMes(id_mes) {
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiempre", "Octubre",
        "Noviembre", "Diciembre"
    ];
    return meses[id_mes];
}

function drawTable(sucursales, ciudades, select) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Mes');
    data.addColumn('number', 'Venta');
    if (select == 1) {
        datosEmpleado(sucursales);
        document.getElementById('Nombre').innerHTML = infoEmpleado.nombreEmpleado;
        document.getElementById('sucursal').innerHTML = infoEmpleado.nombreSucursal;
        data.addRows(infoEmpleado.arrayVentas);
    } else if (select == 2) {
        DatosSucursales(sucursales, ciudades);
        document.getElementById('Nombre').innerHTML = infoSucursal.nombreSucursal;
        document.getElementById('ciudad').innerHTML = infoSucursal.nombreCiudad;
        data.addRows(infoSucursal.arrayVentaSucursal);
    } else {
        DatosEstadisticas(sucursales, ciudades);
        data.addRows(array);
    }
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, { showRowNumber: false, width: '100%', height: '100%' });
}

function drawGrafica(select) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Mes');
    data.addColumn('number', 'Venta');
    if (select == 1) {
        data.addRows(infoEmpleado.arrayVentas);
    } else if (select == 2) {
        data.addRows(infoSucursal.arrayVentaSucursal);
    } else {
        data.addRows(array);
    }

    var table = new google.visualization.ColumnChart(document.getElementById('grafica_div'));
    table.draw(data, { showRowNumber: false, width: '100%', height: '100%' });
}